export const template = {
	html: `<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0;">
 	<meta name="format-detection" content="telephone=no"/>


	<style>
/* Reset styles */ 
body { margin: 0; padding: 0; min-width: 100%; width: 100% !important; height: 100% !important;}
body, table, td, div, p, a { -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%; }
table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; border-spacing: 0; }
img { border: 0; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
#outlook a { padding: 0; }
.ReadMsgBody { width: 100%; } .ExternalClass { width: 100%; }
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }

/* Rounded corners for advanced mail clients only */ 
@media all and (min-width: 560px) {
	.container { border-radius: 8px; -webkit-border-radius: 8px; -moz-border-radius: 8px; -khtml-border-radius: 8px; }
}

/* Set color for auto links (addresses, dates, etc.) */ 
a, a:hover {
	color: #FFFFFF;
}
.footer a, .footer a:hover {
	color: #828999;
}

 	</style>

	<!-- MESSAGE SUBJECT -->
	<title>Sensei newsletter #X</title>

</head>

<!-- BODY -->
<!-- Set message background color (twice) and text color (twice) -->
<body topmargin="0" rightmargin="0" bottommargin="0" leftmargin="0" marginwidth="0" marginheight="0" width="100%" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%; height: 100%; -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%;
	background-color: #2D3445;
	color: #FFFFFF;"
	bgcolor="#2D3445"
	text="#FFFFFF">

<!-- SECTION / BACKGROUND -->
<!-- Set message background color one again -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;" class="background"><tr><td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;"
	bgcolor="#2D3445">

    <table border="0" cellpadding="0" cellspacing="0" align="center"
	width="600" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 600px;" class="wrapper">
	<tr>
		<td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 17px; font-weight: 400; line-height: 160%;
			padding-top: 0; 
			font-family: sans-serif;" class="paragraph">
				

    [SLOT]
    </td></tr></table>
<!-- End of SECTION / BACKGROUND -->
</td></tr></table>

</body>
</html>`,
	heroImage: `

	<!-- HERO IMAGE -->
	<!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2 (wrapper x2). Do not set height for flexible images (including "auto"). -->
	<table border="0" cellpadding="0" cellspacing="0" align="center"
	width="600" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: 100%;
	max-width: 600px;" class="wrapper">
    <tr>
		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
			padding-top: 50px;padding-bottom: 20px" class="hero"><a target="_blank" style="text-decoration: none;"
			href="https://teams.microsoft.com/_#/conversations/Senseis%20-%20public?threadId=19:eda8c8e0f9884f35b390e6eb008fd307@thread.tacv2&ctx=channel"><img border="0" vspace="0" hspace="0"
			src="https://cn-sensei-nl.netlify.app/logo-c.png"
			alt="Please enable images to view this content" title="Hero Image"
			width="340" style="
			width: 100%;
			max-width: 340px;
			color: #FFFFFF; font-size: 13px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;"/></a></td>
	</tr>
    </table>
	`,
	h1: `
    <table border="0" cellpadding="0" cellspacing="0" align="center"
	width="600" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: 100%;
	max-width: 600px;" class="wrapper">
	<tr>
		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;  idth: 100%; font-size: 24px; font-weight: bold; line-height: 130%;
			padding-top: 5px;
			color: #FFFFFF;
			font-family: sans-serif;" class="header">
				
	[SLOT]
		</td>
	</tr></table>
	`,
	h2: `
    <table border="0" cellpadding="0" cellspacing="0" align="center"
	width="600" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: 100%;
	max-width: 600px;" class="wrapper">
	<tr>
		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; idth: 100%; font-size: 20px; font-weight: 400; line-height: 150%; letter-spacing: 2px;
			padding-top: 27px;
			padding-bottom: 0;
			color: #FFFFFF;
			font-family: sans-serif;" class="supheader">
            [SLOT]
		</td>
	</tr></table>`,
	h3: `
    <table border="0" cellpadding="0" cellspacing="0" align="center"
	width="600" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: 100%;
	max-width: 600px;" class="wrapper">
	<tr>
		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; idth: 100%; font-size: 18px; font-weight: 400; line-height: 150%; letter-spacing: 2px;
			padding-top: 27px;
			padding-bottom: 0;
			color: #FFFFFF;
			font-family: sans-serif;" class="supheader">
            [SLOT]
		</td>
	</tr></table>`,
	paragraph: `
    <table border="0" cellpadding="0" cellspacing="0" align="center"
	width="600" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: 100%;
	max-width: 600px;" class="wrapper">
	<tr>
		<td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; idth: 100%; font-size: 17px; font-weight: 400; line-height: 160%;
			padding-top: 15px; 
			color: #FFFFFF;
			font-family: sans-serif;" class="paragraph">
				[SLOT]
		</td>
	</tr></table>`,
	blockquote: `
    <table border="0" cellpadding="0" cellspacing="0" align="center"
	width="600" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: 100%;
	max-width: 600px;" class="wrapper">
	<tr>
		<td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; idth: 100%; font-size: 17px; font-weight: 400; line-height: 160%;
			padding-top: 15px; 
            
			font-family: sans-serif;" class="paragraph">
				
            
            <table border="0" cellpadding="0" cellspacing="0" align="center"
            width="600" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: 100%;
            max-width: 600px;" class="wrapper">
            <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 0; padding-right: 0; width: 100%; font-size: 17px; font-weight: 400; line-height: 160%;
                    padding-left: 10px; 
                    padding-right: 10px; 
                    background-color: #FFFFFF;
                    color:#2D3445;
                    font-family: sans-serif;" class="paragraph">
                        [SLOT]
                </td>
            </tr></table>

		</td>
	</tr></table>`,

	table: `
    <table border="0" cellpadding="0" cellspacing="0" align="center"
	width="600" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: 100%;
	max-width: 600px;" class="wrapper">
	<tr>
		<td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; font-size: 13px; font-weight: 400; line-height: 160%;
			padding-top: 15px; 
			color: #FFFFFF;
			font-family: sans-serif;" class="paragraph">
            <table border="1" cellpadding="10" cellspacing="0" align="center"
            width="600"
             style="border-collapse: collapse; border-spacing: 0; padding: 0; width: 100%;" class="wrapper">
             [SLOT]
             </table>
		</td>
	</tr></table>`,
	button: `<!-- BUTTON -->
	<!-- Set button background color at TD, link/text color at A and TD, font family ("sans-serif" or "Georgia, serif") at TD. For verification codes add "letter-spacing: 5px;". Link format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Button-Name}}&utm_campaign={{Campaign-Name}} -->
	<table border="0" cellpadding="0" cellspacing="0" align="center"
	width="600" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: 100%;
	max-width: 600px;" class="wrapper"><tr>

		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; idth: 100%;
			padding-top: 25px;
			padding-bottom: 5px;" class="button"><a
			href="https://teams.microsoft.com/_#/conversations/Senseis%20-%20public?threadId=19:eda8c8e0f9884f35b390e6eb008fd307@thread.tacv2&ctx=channel" target="_blank" style="text-decoration: underline;">
				<table border="0" cellpadding="0" cellspacing="0" align="center" style="max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;"><tr><td align="center" valign="middle" style="padding: 12px 24px; margin: 0; text-decoration: underline; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;"
					bgcolor="#E9703E"><a target="_blank" style="text-decoration: underline;
					color: #FFFFFF; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 120%;"
					href="https://teams.microsoft.com/_#/conversations/Senseis%20-%20public?threadId=19:eda8c8e0f9884f35b390e6eb008fd307@thread.tacv2&ctx=channel">
						View on GitHub
					</a>
			</td></tr></table></a>
		</td>
	</tr></table>`,

	line: `<!-- LINE -->
	<!-- Set line color -->
    <table border="0" cellpadding="0" cellspacing="0" align="center"
	width="600" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: 100%;
	max-width: 600px;" class="wrapper">
	<tr>
		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; idth: 100%;
			padding-top: 30px;" class="line"><hr
			color="#565F73" align="center" width="100%" size="1" noshade style="margin: 0; padding: 0;" />
		</td>
	</tr></table>`,
	footer: `<!-- FOOTER -->
	<!-- Set text color and font family ("sans-serif" or "Georgia, serif"). Duplicate all text styles in links, including line-height -->
    <table border="0" cellpadding="0" cellspacing="0" align="center"
	width="600" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: 100%;
	max-width: 600px;" class="wrapper">
    <tr>
		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; idth: 100%; font-size: 13px; font-weight: 400; line-height: 150%;
			padding-top: 10px;
			padding-bottom: 20px;
			color: #828999;
			font-family: sans-serif;" class="footer">

				This email was sent to&nbsp;you because you are part of CN web department.				
		</td>
	</tr>
    </table>`,
	code: '<span style="padding-left: 5px; padding-right: 5px;background-color: #CCCCCC;color:#2D3445">[SLOT]</span>'
};
