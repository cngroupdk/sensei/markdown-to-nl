export const nl = `# Sensei Newsletter no 1.

> List of recommended VS Code plugins (+ one extra for Vim)


Hi, as voted up in Sensei survey, here is our first Sensei newsletter.
It should bring you news from the web programming world.


----------

## Topics for this newsletter are as follows: 

1. What/who are Senseis
2. React project skeleton
3. News From React Conf 2021
4. List of interesting articles and news which we recommend to check


## 1. What is Sensei group


Sensei is group of senior Web developrs in CN Web Department. The group has been here for quite some time, but rather hidden to regular developers. Focus of the group has been education, technology 'lab', project reviews, etc. Unfortunately Sensei hadn't been performing great in past few years and it was decided to restart it last summer with fresh members on board.

Current crew is as follows:

- Dominik Mikuš - vue, angular, node
- Lukáš Kadoch - react, node
- Lukáš Duspiva - react, node
- Tomáš Horáček - React, React Native, iOS, elm
- Tomáš Řezáč - Angular, Svelte
- Petr Čaněk - node, react
- Rado Holík - android
- Jakub Řičař - react, reacnative
- Lukáš Papáy - PHP, vue

After the restart we focus more on knowledge sharing and education and less on project reviews. We are going to organize meetups for CN developers. Compared to Indeed, we try to present in bit more hands on approach. Also we try to deliver what you consider important for you. Based on survey results it s Typescript, NX monorepo, JS basics to Advanced...


## 2. React Project Seleton

Senseis have something special for you this time. Many JavaScript/TypeScript projects have to setup tools like ESLint, Prettier, NPM scripts, and EditorConfig.

We have prepared [example NPM project setup](https://gitlab.com/cngroupdk/sensei/public-materials/-/tree/main/npm-setup-example) (with [detailed explanation](https://gitlab.com/cngroupdk/sensei/public-materials/-/tree/main/npm-setup-example#npm-project-setup)). Feel free to adopt it on your projects, or [open issue](https://gitlab.com/cngroupdk/sensei/public-materials/-/issues) if you have questions, comments, suggestions, ...

## 3. React Conf 2021

On December the 8th, last year's React Conf took a place. From the number of interesting topics featuring React 18, which we recommend all to watch, we would like to highlight the  **React without memo** talk, where Xuan Huang presented the problem of providing a good user experience backed by a code with a good developer experience. As a possible solution, he reveals a prototype of \`React Forget\` - a compiler capable of automatically adding memoization for components. From what we have seen, using this compiler significantly changes the way how production-ready React code looks, making the source code free of many performance optimization constructs. You can watch records from the whole conference at [conf.reactjs.org](https://conf.reactjs.org/). If you prefer the article, you can visit [React Conf 2021 Recap](https://reactjs.org/blog/2021/12/17/react-conf-2021-recap.html).

## 4. Articles & News

- There is a new [TypeScript version 4.5](https://devblogs.microsoft.com/typescript/announcing-typescript-4-5/). Go check it out!
- Do you want to know more about [Vite's ecosystem](https://patak.dev/vite/ecosystem.html)?
- Do you need a quick runtime validation in node.js? [JSONSchema validation](https://simonplend.com/get-started-with-validation-in-node-js/) got you.
- Are you too lazy to play with node.js locally? Try stackblitz's [node.new](https://node.new). 
- Are you a Vue developer? Are you using the Composition API? Check out [VueUse](https://vueuse.org/). A very useful collection of Vue composition utilities.
- [Container query polyfill](https://github.com/GoogleChromeLabs/container-query-polyfill) is here
- [Advanced git series](https://css-tricks.com/using-the-reflog-to-restore-lost-commits/)	(Advanced)
- [Clean Architecture on Frontend](https://dev.to/bespoyasov/clean-architecture-on-frontend-4311) Loong article on various topics regarding FE architecture and business requirements (Advanced)
- Next 12 and basic info about it [Next 12](https://nextjs.org/blog/next-12) (Advanced) 
- [All modern JavaScript features added in last 10 years](https://turriate.com/articles/modern-javascript-everything-you-missed-over-10-years) in one concise list. Are you sure that you know about all of them? 	 😜	(Beginner - Mid)
- [100 things Dan Abramov learned working on the React team](https://mobile.twitter.com/dan_abramov/status/1470613731071696896) in concise 100 tweets short form. Interesting also for non-React developers, e.g. insight from OSS management like "Most PRs create more work, than it solve" ( ͡° ͜ʖ ͡°) (All)
- Do you know what is a [difference between](https://thisthat.dev) \`:active\` and \`:focus\`, \`<button>\` and \`<input type="button" />, or \`for ... in\` and \`for ... of\`? Use this simple site to learn about many nuances of CSS, HTML, JS, TS, and more. (Beginner - Advanced)


## 5a. VS Code Plugins

Here we come with a list great plugins you should try if you use VS Code. If you use some other great plugins please share them with us in Sensei chanel

| Name                 | Technology            |                                                              |  
| -------------------------------- | ------------------ | ------------------------------------------------------------ |
| [Angular snippets](https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2)                | Angular            |                                                              |  
| [Apollo GraphQL](https://marketplace.visualstudio.com/items?itemName=apollographql.vscode-apollo)                   | GraphQL            | Rich editor support for GraphQL                                                             |  
| [Color Highlight](https://marketplace.visualstudio.com/items?itemName=naumovs.color-highlight)                  | any                |                                                              |  
| [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)         | any                | This plugin attempts to override user/workspace settings with settings found in .editorconfig files |  
| [Git History](https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory)                      | Git                |                                                              |  
| [GitLens — Git supercharged](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)       | Git                | Quickly glimpse into whom, why, and when a line or code block was changed. |  
| [indent-rainbow](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow)                   | any                | This extension colorizes the indentation in front of your text alternating four different colors on each step. |  
| [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)              | Markdown           |                                                              |  
| [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)       | Markdown           |                                                              |  
| [Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)              | Any                   |                                                              |  
| [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)        |                    | For projects without prettier in CLI                         |  
| [Tabnine](https://marketplace.visualstudio.com/items?itemName=TabNine.tabnine-vscode)                          | AI autocomplete    |                                                              |  
| [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)                           |                    |                                                              |  
| [Import Cost](https://marketplace.visualstudio.com/items?itemName=wix.vscode-import-cost)                      | Any                   |                                                              |  
| [Live Share](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare)                       |  Any                  | Real-time collaborative development from the comfort of your favorite tools. |  
| [Markdown Preview Mermaid Support](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid) | Mermaid / Markdown | Adds Mermaid diagram and flowchart support to VS Code's builtin markdown preview |  
| [MDX](https://marketplace.visualstudio.com/items?itemName=silvenon.mdx)                              | JSX / Markdown     | Provides syntax highlighting and bracket matching for MDX (JSX in Markdown) files. |  
| [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)                            | Vue3               | Language support for Vue 3                                   |  
| [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)                      |  Any                  |                                                              |  

## 5b. VIM Plugins

| Name                 | Technology            |                                                              |  
| ------- | ----------------------------------------- | ------------------------------------------------------------ |
| [coc.vim](https://github.com/neoclide/coc.nvim) | JS, TS, Vue, Go, CSS, PHP and many others | Make you Vim/Neovim as smart as VSCode with language servers IntelliSense™️ autocomplete. Same as in your other favorite editor! |
`;
